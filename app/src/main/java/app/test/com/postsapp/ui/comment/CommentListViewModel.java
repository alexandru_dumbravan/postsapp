package app.test.com.postsapp.ui.comment;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import app.test.com.postsapp.api.NetworkConfigurator;
import app.test.com.postsapp.api.model.Comment;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.fetchers.CommentDataFetcher;
import app.test.com.postsapp.fetchers.FetcherCallback;
import app.test.com.postsapp.persistence.db.AppDatabase;

public class CommentListViewModel extends AndroidViewModel {

    private CommentDataFetcher commentDataFetcher;
    private MutableLiveData<List<Comment>> commentLiveData;
    private boolean isRemoteDataLoaded;

    public CommentListViewModel(Application application) {
        this(application, new NetworkConfigurator().init(), AppDatabase.getInstance(application));
    }


    CommentListViewModel(Application application, NetworkConfigurator networkConfigurator, AppDatabase appDatabase) {
        this(application, new CommentDataFetcher(networkConfigurator.createService(PostApi.class), appDatabase.commentDao()),
                new MutableLiveData<List<Comment>>());
    }


    CommentListViewModel(Application application, CommentDataFetcher commentDataFetcher, MutableLiveData<List<Comment>> commentLiveData) {
        super(application);
        this.commentDataFetcher = commentDataFetcher;
        this.commentLiveData = commentLiveData;
    }

    public MutableLiveData<List<Comment>> getCommentLiveData() {
        return commentLiveData;
    }


    public void loadRemoteData(long postId) {
        commentDataFetcher.fetchRemoteCommentData(postId, new FetcherCallback<Void>() {
            @Override
            public void onDataLoaded(Void data) {
                isRemoteDataLoaded = true;
            }

            @Override
            public void onError() {

            }
        });
    }


    public void attemptToLoadRemoteData(long postId) {
        if (!isRemoteDataLoaded) {
            loadRemoteData(postId);
        }
    }


    public void loadLocalData(long postId) {
        commentDataFetcher.fetchLocalData(postId, new FetcherCallback<List<Comment>>() {
            @Override
            public void onDataLoaded(List<Comment> data) {
                commentLiveData.postValue(data);
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        commentDataFetcher.clearAll();
    }
}
