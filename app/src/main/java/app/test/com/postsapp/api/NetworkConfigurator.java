package app.test.com.postsapp.api;


import android.support.annotation.VisibleForTesting;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkConfigurator {

    public static String HOST = "https://jsonplaceholder.typicode.com";
    private Retrofit retrofit;


    public NetworkConfigurator init() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return this;
    }

    public <T> T createService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }

    @VisibleForTesting
    public Retrofit getRetrofit() {
        return retrofit;
    }
}
