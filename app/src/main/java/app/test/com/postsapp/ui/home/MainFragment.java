package app.test.com.postsapp.ui.home;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.test.com.postsapp.R;
import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.ui.comment.CommentActivity;

public class MainFragment extends LifecycleFragment implements SwipeRefreshLayout.OnRefreshListener {

    private MainViewModel mainViewModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MainAdapter mainAdapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.attemptLoadRemoteData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mainAdapter = new MainAdapter(new ArrayList<Post>(), new OnRowClickListener());
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_main_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mainAdapter);
        mainViewModel.getErrorLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                Toast.makeText(getContext(), R.string.error_generic, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        mainViewModel.getPostsLiveData().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                mainAdapter.clear();
                mainAdapter.addAll(posts);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragment_main_srl);
        swipeRefreshLayout.setOnRefreshListener(this);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewModel.loadLocalData();
    }

    @Override
    public void onRefresh() {
        mainViewModel.loadRemoteData();
    }


    private class OnRowClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Post post = (Post) v.getTag();
            startActivity(CommentActivity.buildIntent(getContext(), post.getId()));
        }
    }
}
