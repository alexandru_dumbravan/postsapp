package app.test.com.postsapp.ui.comment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import app.test.com.postsapp.BuildConfig;
import app.test.com.postsapp.R;
import app.test.com.postsapp.ui.comment.detail.PostDetailFragment;

public class CommentActivity extends AppCompatActivity {

    public static final String EXTRA_POST_ID = BuildConfig.APPLICATION_ID + ".post.extras.POST_ID";

    public static Intent buildIntent(Context context, long postId) {
        Intent intent = new Intent(context, CommentActivity.class);
        intent.putExtra(EXTRA_POST_ID, postId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        long postId = getIntent().getLongExtra(EXTRA_POST_ID, -1);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.activity_comment_post_detail_container, PostDetailFragment.newInstance(postId))
                    .replace(R.id.activity_comment_list_container, CommentListFragment.newInstance(postId))
                    .commit();
        }
    }
}
