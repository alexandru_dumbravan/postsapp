package app.test.com.postsapp.fetchers;


import java.util.List;

import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.persistence.db.PostDao;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PostDataFetcher extends DataFetcher {

    private PostApi postApi;
    private PostDao postDao;
    private FetcherCallback<Void> onRemoteDataCallback;
    private FetcherCallback<List<Post>> onLocalDataCallback;
    private FetcherCallback<Post> onSinglePostCallback;
    private Disposable remoteDisposable;

    public PostDataFetcher(PostApi postApi, PostDao postDao) {
        this.postApi = postApi;
        this.postDao = postDao;
    }

    public void fetchRemotePostData(FetcherCallback<Void> onErrorCallback) {
        onRemoteDataCallback = onErrorCallback;
        //Let this disposable leak until the request finishes so that it will be disposed
        remoteDisposable = postApi.getPosts().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Post>>() {
            @Override
            public void accept(@NonNull List<Post> posts) throws Exception {
                insertPosts(posts);
                if (remoteDisposable != null) {
                    remoteDisposable.dispose();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                throwable.printStackTrace();
                if (onRemoteDataCallback != null) {
                    onRemoteDataCallback.onError();
                }
                if (remoteDisposable != null) {
                    remoteDisposable.dispose();
                }

            }
        });
    }

    private void insertPosts(final List<Post> posts) {
        Single.create(new SingleOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<Integer> e) throws Exception {
                postDao.insertPosts(posts.toArray(new Post[posts.size()]));
                e.onSuccess(0);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    public void fetchLocalPostData(FetcherCallback<List<Post>> onLocalDataCallback) {
        this.onLocalDataCallback = onLocalDataCallback;
        addDisposable(postDao.getAllPosts().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Post>>() {
            @Override
            public void accept(@NonNull List<Post> posts) throws Exception {
                if (PostDataFetcher.this.onLocalDataCallback != null) {
                    PostDataFetcher.this.onLocalDataCallback.onDataLoaded(posts);
                }
            }
        }));
    }

    public void fetchPostById(long postId, final FetcherCallback<Post> onSinglePostCallback) {
        this.onSinglePostCallback = onSinglePostCallback;
        addDisposable(postDao.getPostForId(postId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Post>() {
            @Override
            public void accept(@NonNull Post post) throws Exception {
                if (onSinglePostCallback != null) {
                    PostDataFetcher.this.onSinglePostCallback.onDataLoaded(post);
                }
            }
        }));
    }

    @Override
    public void clearAll() {
        super.clearAll();
        onRemoteDataCallback = null;
        onLocalDataCallback = null;
        onSinglePostCallback = null;
    }
}
