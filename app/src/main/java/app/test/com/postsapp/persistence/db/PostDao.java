package app.test.com.postsapp.persistence.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import org.reactivestreams.Publisher;

import java.util.List;

import app.test.com.postsapp.api.model.Post;
import io.reactivex.Flowable;

@Dao
public interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPosts(Post... posts);

    @Query("Select * from post")
    Flowable<List<Post>> getAllPosts();

    @Query("Select * from post where id=:id")
    Flowable<Post> getPostForId(long id);
}
