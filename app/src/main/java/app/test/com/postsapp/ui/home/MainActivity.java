package app.test.com.postsapp.ui.home;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import app.test.com.postsapp.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.activity_main_container, MainFragment.newInstance()).commit();
        }
    }
}
