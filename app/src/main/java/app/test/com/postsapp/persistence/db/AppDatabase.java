package app.test.com.postsapp.persistence.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import app.test.com.postsapp.api.model.Comment;
import app.test.com.postsapp.api.model.Post;

@Database(entities = {Post.class, Comment.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public synchronized static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context,
                    AppDatabase.class, "app-db").build();
        }
        return instance;
    }

    public abstract PostDao postDao();

    public abstract CommentDao commentDao();
}