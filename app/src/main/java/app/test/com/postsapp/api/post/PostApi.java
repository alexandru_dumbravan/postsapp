package app.test.com.postsapp.api.post;


import java.util.List;

import app.test.com.postsapp.api.model.Comment;
import app.test.com.postsapp.api.model.Post;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface PostApi {

    @GET("/posts")
    Single<List<Post>> getPosts();

    @GET("/posts/{postId}/comments")
    Single<List<Comment>> getPostComments(@Path("postId") long postId);
}
