package app.test.com.postsapp.ui.comment.detail;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;

import app.test.com.postsapp.api.NetworkConfigurator;
import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.fetchers.FetcherCallback;
import app.test.com.postsapp.fetchers.PostDataFetcher;
import app.test.com.postsapp.persistence.db.AppDatabase;

public class PostDetailViewModel extends AndroidViewModel {

    private PostDataFetcher postDataFetcher;
    private MutableLiveData<Post> postLiveData;


    public PostDetailViewModel(Application application) {
        this(application, new NetworkConfigurator().init(), AppDatabase.getInstance(application));
    }


    PostDetailViewModel(Application application, NetworkConfigurator networkConfigurator, AppDatabase appDatabase) {
        this(application, new PostDataFetcher(networkConfigurator.createService(PostApi.class), appDatabase.postDao()),
                new MutableLiveData<Post>());
    }


    PostDetailViewModel(Application application, PostDataFetcher postDataFetcher, MutableLiveData<Post> postLiveData) {
        super(application);
        this.postDataFetcher = postDataFetcher;
        this.postLiveData = postLiveData;
    }

    public MutableLiveData<Post> getPostLiveData() {
        return postLiveData;
    }

    public void loadLocalData(long postId) {
        postDataFetcher.fetchPostById(postId, new FetcherCallback<Post>() {
            @Override
            public void onDataLoaded(Post data) {
                postLiveData.postValue(data);
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        postDataFetcher.clearAll();
    }
}
