package app.test.com.postsapp.fetchers;


import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

public class DataFetcher {

    List<Disposable> disposables = new ArrayList<>();


    protected void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    public void clearAll() {
        for (Disposable disposable : disposables) {
            disposable.dispose();
        }
        disposables.clear();
    }


}
