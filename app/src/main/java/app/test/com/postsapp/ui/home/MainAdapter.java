package app.test.com.postsapp.ui.home;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.test.com.postsapp.R;
import app.test.com.postsapp.api.model.Post;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.PostViewHolder> {

    private List<Post> posts = new ArrayList<>();
    private View.OnClickListener onRowClickListener;

    public MainAdapter(List<Post> posts, View.OnClickListener onRowClickListener) {
        this.posts = posts;
        this.onRowClickListener = onRowClickListener;
    }

    public void addAll(List<Post> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }

    public void clear() {
        this.posts.clear();
        notifyDataSetChanged();
    }


    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PostViewHolder postViewHolder = new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_post_item_layout, parent, false));
        postViewHolder.itemView.setOnClickListener(onRowClickListener);
        return postViewHolder;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        Post post = posts.get(position);
        holder.title.setText(post.getTitle());
        holder.body.setText(post.getBody());
        holder.itemView.setTag(post);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    static class PostViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView body;

        PostViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.view_post_item_layout_title_text_view);
            body = (TextView) itemView.findViewById(R.id.view_post_item_layout_body_text_view);
        }
    }
}
