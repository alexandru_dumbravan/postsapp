package app.test.com.postsapp.ui.home;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import app.test.com.postsapp.api.NetworkConfigurator;
import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.fetchers.FetcherCallback;
import app.test.com.postsapp.fetchers.PostDataFetcher;
import app.test.com.postsapp.persistence.db.AppDatabase;

public class MainViewModel extends AndroidViewModel {

    private PostDataFetcher postDataFetcher;
    private MutableLiveData<List<Post>> postsLiveData;
    private MutableLiveData<Boolean> errorLiveData;
    private boolean isRemoteDataLoaded = false;

    public MainViewModel(Application application) {
        this(application, new NetworkConfigurator().init(), AppDatabase.getInstance(application));
    }


    MainViewModel(Application application, NetworkConfigurator networkConfigurator, AppDatabase appDatabase) {
        this(application, new PostDataFetcher(networkConfigurator.createService(PostApi.class), appDatabase.postDao()),
                new MutableLiveData<List<Post>>(), new MutableLiveData<Boolean>());
    }


    MainViewModel(Application application, PostDataFetcher postDataFetcher, MutableLiveData<List<Post>> postsLiveData, MutableLiveData<Boolean> errorLiveData) {
        super(application);
        this.postDataFetcher = postDataFetcher;
        this.postsLiveData = postsLiveData;
        this.errorLiveData = errorLiveData;
    }


    public MutableLiveData<List<Post>> getPostsLiveData() {
        return postsLiveData;
    }

    public MutableLiveData<Boolean> getErrorLiveData() {
        return errorLiveData;
    }

    public void loadLocalData() {
        postDataFetcher.fetchLocalPostData(new FetcherCallback<List<Post>>() {
            @Override
            public void onDataLoaded(List<Post> data) {
                postsLiveData.postValue(data);
            }

            @Override
            public void onError() {
                errorLiveData.postValue(Boolean.FALSE.equals(errorLiveData.getValue()));
            }
        });
    }

    public void attemptLoadRemoteData() {
        if (!isRemoteDataLoaded) {
            loadRemoteData();
        }
    }

    public void loadRemoteData() {
        postDataFetcher.fetchRemotePostData(new FetcherCallback<Void>() {
            @Override
            public void onDataLoaded(Void data) {
                isRemoteDataLoaded = true;
            }

            @Override
            public void onError() {
                errorLiveData.postValue(Boolean.FALSE.equals(errorLiveData.getValue()));
            }
        });
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        postDataFetcher.clearAll();
    }
}
