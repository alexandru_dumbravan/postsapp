package app.test.com.postsapp.fetchers;

public interface FetcherCallback<T> {

    void onDataLoaded(T data);

    void onError();
}
