package app.test.com.postsapp.ui.comment.detail;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.test.com.postsapp.R;
import app.test.com.postsapp.api.model.Post;

public class PostDetailFragment extends LifecycleFragment {

    private static final String ARG_POST_ID = "postId";

    public static PostDetailFragment newInstance(long postId) {
        PostDetailFragment postDetailFragment = new PostDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_POST_ID, postId);
        postDetailFragment.setArguments(bundle);
        return postDetailFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_detail, container, false);

        final TextView title = (TextView) view.findViewById(R.id.fragment_post_detail_title_text);
        final TextView body = (TextView) view.findViewById(R.id.fragment_post_detail_body_text);

        PostDetailViewModel postDetailViewModel = ViewModelProviders.of(this).get(PostDetailViewModel.class);
        postDetailViewModel.getPostLiveData().observe(this, new Observer<Post>() {
            @Override
            public void onChanged(@Nullable Post post) {
                if (post != null) {
                    title.setText(post.getTitle());
                    body.setText(post.getBody());
                }
            }
        });
        postDetailViewModel.loadLocalData(getArguments().getLong(ARG_POST_ID, -1));
        return view;
    }


}
