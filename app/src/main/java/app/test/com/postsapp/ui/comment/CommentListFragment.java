package app.test.com.postsapp.ui.comment;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.test.com.postsapp.R;
import app.test.com.postsapp.api.model.Comment;

public class CommentListFragment extends LifecycleFragment {

    private static final String ARG_POST_ID = "postId";
    private CommentListViewModel commentListViewModel;
    private long postId;

    public static CommentListFragment newInstance(long postId) {
        CommentListFragment commentListFragment = new CommentListFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_POST_ID, postId);
        commentListFragment.setArguments(bundle);
        return commentListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postId = getArguments().getLong(ARG_POST_ID, -1);
        commentListViewModel = ViewModelProviders.of(this).get(CommentListViewModel.class);
        commentListViewModel.attemptToLoadRemoteData(postId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment_list, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_comment_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        final CommentAdapter commentAdapter = new CommentAdapter(new ArrayList<Comment>());
        recyclerView.setAdapter(commentAdapter);
        commentListViewModel.getCommentLiveData().observe(this, new Observer<List<Comment>>() {
            @Override
            public void onChanged(@Nullable List<Comment> comments) {
                commentAdapter.addAll(comments);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        commentListViewModel.loadLocalData(postId);
    }
}
