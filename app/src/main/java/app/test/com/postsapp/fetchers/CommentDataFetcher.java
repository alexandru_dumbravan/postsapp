package app.test.com.postsapp.fetchers;


import java.util.List;

import app.test.com.postsapp.api.model.Comment;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.persistence.db.CommentDao;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CommentDataFetcher extends DataFetcher {

    private PostApi postApi;
    private CommentDao commentDao;
    private FetcherCallback<Void> onRemoteDataCallback;
    private FetcherCallback<List<Comment>> onLocalDataCallback;
    private Disposable remoteDisposable;

    public CommentDataFetcher(PostApi postApi, CommentDao commentDao) {
        this.postApi = postApi;
        this.commentDao = commentDao;
    }

    public void fetchRemoteCommentData(long postId, FetcherCallback<Void> onRemoteDataCallback) {
        this.onRemoteDataCallback = onRemoteDataCallback;
        remoteDisposable = postApi.getPostComments(postId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Comment>>() {
            @Override
            public void accept(@NonNull List<Comment> comments) throws Exception {
                insertComments(comments);
                if (remoteDisposable != null) {
                    remoteDisposable.dispose();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                throwable.printStackTrace();
                if (CommentDataFetcher.this.onRemoteDataCallback != null) {
                    CommentDataFetcher.this.onRemoteDataCallback.onError();
                }
                if (remoteDisposable != null) {
                    remoteDisposable.dispose();
                }
            }
        });
    }

    private void insertComments(final List<Comment> comments) {
        Single.create(new SingleOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<Integer> e) throws Exception {
                commentDao.insertComments(comments.toArray(new Comment[comments.size()]));
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }


    public void fetchLocalData(long postId, final FetcherCallback<List<Comment>> onLocalDataCallback) {
        this.onLocalDataCallback = onLocalDataCallback;
        addDisposable(commentDao.getCommentsForPostId(postId).subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Comment>>() {
            @Override
            public void accept(@NonNull List<Comment> comments) throws Exception {
                if (CommentDataFetcher.this.onLocalDataCallback != null) {
                    onLocalDataCallback.onDataLoaded(comments);
                }
            }
        }));

    }

    @Override
    public void clearAll() {
        super.clearAll();
        onLocalDataCallback = null;
        onRemoteDataCallback = null;
    }
}
