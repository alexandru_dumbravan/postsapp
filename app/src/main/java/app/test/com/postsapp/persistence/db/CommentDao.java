package app.test.com.postsapp.persistence.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import app.test.com.postsapp.api.model.Comment;
import io.reactivex.Flowable;


@Dao
public interface CommentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertComments(Comment... comments);

    @Query("Select * from comment where postId=:postId")
    Flowable<List<Comment>> getCommentsForPostId(long postId);

}
