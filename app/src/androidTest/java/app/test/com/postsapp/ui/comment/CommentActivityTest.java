package app.test.com.postsapp.ui.comment;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.core.deps.guava.base.Charsets;
import android.support.test.espresso.core.deps.guava.io.Resources;
import android.support.test.espresso.core.deps.guava.reflect.TypeToken;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.net.URL;
import java.util.List;

import app.test.com.postsapp.MatcherUtils;
import app.test.com.postsapp.R;
import app.test.com.postsapp.api.NetworkConfigurator;
import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.persistence.db.AppDatabase;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class CommentActivityTest {

    @Rule
    public ActivityTestRule<CommentActivity> testRule = new ActivityTestRule<CommentActivity>(CommentActivity.class, false, false) {
        @Override
        protected Intent getActivityIntent() {
            return CommentActivity.buildIntent(InstrumentationRegistry.getInstrumentation().getTargetContext(), 1);
        }
    };
    private MockWebServer server;


    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();


        URL url = Resources.getResource("post_1_comments.json");
        String text = Resources.toString(url, Charsets.UTF_8);
        // Schedule some responses.
        server.enqueue(new MockResponse().setBody(text));
        ;
        // Start the server.
        server.start();
        NetworkConfigurator.HOST = server.url("").toString();

        //Need to ensure the posts are persisted before the test runs
        url = Resources.getResource("posts.json");
        text = Resources.toString(url, Charsets.UTF_8);
        Gson gson = new GsonBuilder().create();
        List<Post> posts = gson.fromJson(text, new TypeToken<List<Post>>() {
        }.getType());

        AppDatabase.getInstance(InstrumentationRegistry.getInstrumentation().getTargetContext().getApplicationContext()).postDao().insertPosts(posts.toArray(new Post[posts.size()]));



        testRule.launchActivity(null);
    }

    @Test
    public void testCommentDataIsVisible() {

        Espresso.onView(ViewMatchers.withId(R.id.fragment_comment_list_recycler_view)).check(ViewAssertions.matches(MatcherUtils.atPosition(0, ViewMatchers.hasDescendant(ViewMatchers.withText("Eliseo@gardner.biz")))));
        Espresso.onView(ViewMatchers.withId(R.id.fragment_comment_list_recycler_view)).check(ViewAssertions.matches(MatcherUtils.atPosition(0, ViewMatchers.hasDescendant(ViewMatchers.withText("Test Name")))));
        Espresso.onView(ViewMatchers.withId(R.id.fragment_comment_list_recycler_view)).check(ViewAssertions.matches(MatcherUtils.atPosition(0, ViewMatchers.hasDescendant(ViewMatchers.withText("Fit Body")))));
    }

    @Test
    public void testPostDataIsVisible() throws Exception{

        Espresso.onView(ViewMatchers.withText("Title")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withText("Really short message to test")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));

    }


    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

}