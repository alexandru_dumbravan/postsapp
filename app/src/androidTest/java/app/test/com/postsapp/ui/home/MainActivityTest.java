package app.test.com.postsapp.ui.home;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.core.deps.guava.base.Charsets;
import android.support.test.espresso.core.deps.guava.io.Resources;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.ComponentNameMatchers;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.net.URL;

import app.test.com.postsapp.MatcherUtils;
import app.test.com.postsapp.R;
import app.test.com.postsapp.api.NetworkConfigurator;
import app.test.com.postsapp.ui.comment.CommentActivity;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;


public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<MainActivity>(MainActivity.class, false, false);

    private MockWebServer server;

    @Before
    public void setUp() throws Exception {

        server = new MockWebServer();
        URL url = Resources.getResource("posts.json");
        String text = Resources.toString(url, Charsets.UTF_8);
        // Schedule some responses.
        server.enqueue(new MockResponse().setBody(text));
        ;
        // Start the server.
        server.start();
        NetworkConfigurator.HOST = server.url("").toString();
        testRule.launchActivity(null);
        Intents.init();
    }


    @Test
    public void testDataIsVisible() {
        Espresso.onView(ViewMatchers.withId(R.id.fragment_main_recycler_view)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.fragment_main_recycler_view)).check(ViewAssertions.matches(MatcherUtils.atPosition(0, ViewMatchers.hasDescendant(ViewMatchers.withText("Title")))));
        Espresso.onView(ViewMatchers.withId(R.id.fragment_main_recycler_view)).check(ViewAssertions.matches(MatcherUtils.atPosition(0, ViewMatchers.hasDescendant(ViewMatchers.withText("Really short message to test")))));
    }

    @Test
    public void testClickOnListItem() {
        Espresso.onView(ViewMatchers.withId(R.id.fragment_main_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, ViewActions.click()));
        Intents.intended(Matchers.allOf(IntentMatchers.hasComponent(ComponentNameMatchers.hasClassName(CommentActivity.class.getName())), IntentMatchers.hasExtra(CommentActivity.EXTRA_POST_ID, 1L)));
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
        Intents.release();
    }

}