package app.test.com.postsapp.ui.comment.detail;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.fetchers.FetcherCallback;
import app.test.com.postsapp.fetchers.PostDataFetcher;

@RunWith(MockitoJUnitRunner.class)
public class PostDetailViewModelTest {

    private PostDetailViewModel subject;
    @Mock
    private Application application;
    @Mock
    private PostDataFetcher postDataFetcher;
    @Mock
    private MutableLiveData<Post> mutableLiveData;

    @Before
    public void setUp() throws Exception {
        subject = new PostDetailViewModel(application, postDataFetcher, mutableLiveData);
    }


    @Test
    public void loadLocalData() throws Exception {
        long postId = 1;
        final Post post = new Post();
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<Post> fetcherCallback = (FetcherCallback<Post>) invocation.getArguments()[1];
                fetcherCallback.onDataLoaded(post);
                return null;
            }
        }).when(postDataFetcher).fetchPostById(Mockito.anyInt(), Mockito.<FetcherCallback<Post>>any());
        subject.loadLocalData(postId);
        Mockito.verify(postDataFetcher).fetchPostById(Mockito.eq(postId), (FetcherCallback<Post>) Mockito.any());
        Mockito.verify(mutableLiveData).postValue(post);
    }

    @Test
    public void onCleared() throws Exception {
        subject.onCleared();
        Mockito.verify(postDataFetcher).clearAll();
    }

}