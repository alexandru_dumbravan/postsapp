package app.test.com.postsapp.fetchers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import app.test.com.postsapp.api.model.Comment;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.persistence.db.CommentDao;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

@RunWith(MockitoJUnitRunner.class)
public class CommentDataFetcherTest {

    private CommentDataFetcher subject;
    @Mock
    private PostApi postApi;
    @Mock
    private CommentDao commentDao;
    private long postId = 1;


    @Before
    public void setUp() throws Exception {
        subject = new CommentDataFetcher(postApi, commentDao);
        RxJavaPlugins.reset();
        RxAndroidPlugins.reset();
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });

        RxJavaPlugins.setIoSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Scheduler scheduler) throws Exception {
                return Schedulers.trampoline();
            }
        });


    }

    @Test
    public void fetchRemoteCommentData_success() throws Exception {
        FetcherCallback<Void> fetcherCallback = Mockito.mock(FetcherCallback.class);
        final List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());
        Single<List<Comment>> remoteSingle = Single.just(comments);
        Mockito.when(postApi.getPostComments(Mockito.anyInt())).thenReturn(remoteSingle);

        subject.fetchRemoteCommentData(postId, fetcherCallback);
        Mockito.verify(postApi).getPostComments(postId);
        Mockito.verify(commentDao).insertComments(comments.toArray(new Comment[comments.size()]));

    }


    @Test
    public void fetchRemoteCommentData_error() throws Exception {
        FetcherCallback<Void> fetcherCallback = Mockito.mock(FetcherCallback.class);
        Single<List<Comment>> remoteSingle = Single.fromCallable(new Callable<List<Comment>>() {
            @Override
            public List<Comment> call() throws Exception {
                throw new RuntimeException("test");
            }
        });
        Mockito.when(postApi.getPostComments(Mockito.anyInt())).thenReturn(remoteSingle);

        subject.fetchRemoteCommentData(postId, fetcherCallback);
        Mockito.verify(postApi).getPostComments(postId);
        Mockito.verify(fetcherCallback).onError();

    }

    @Test
    public void fetchLocalData() throws Exception {
        FetcherCallback<List<Comment>> fetcherCallback = Mockito.mock(FetcherCallback.class);
        final List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());
        Flowable<List<Comment>> flowable = Flowable.just(comments);
        Mockito.when(commentDao.getCommentsForPostId(Mockito.anyInt())).thenReturn(flowable);
        subject.fetchLocalData(postId, fetcherCallback);
        Mockito.verify(commentDao).getCommentsForPostId(postId);
        Mockito.verify(fetcherCallback).onDataLoaded(comments);
    }


}