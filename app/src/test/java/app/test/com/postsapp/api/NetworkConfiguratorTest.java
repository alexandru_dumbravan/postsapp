package app.test.com.postsapp.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import app.test.com.postsapp.api.post.PostApi;


public class NetworkConfiguratorTest {

    private NetworkConfigurator subject;

    @Before
    public void setUp() throws Exception {
        subject = new NetworkConfigurator();
    }

    @Test
    public void init() throws Exception {
        subject.init();
        Assert.assertNotNull(subject.getRetrofit());
    }

}