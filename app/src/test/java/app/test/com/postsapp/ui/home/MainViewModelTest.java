package app.test.com.postsapp.ui.home;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.fetchers.FetcherCallback;
import app.test.com.postsapp.fetchers.PostDataFetcher;

@RunWith(MockitoJUnitRunner.class)
public class MainViewModelTest {

    private MainViewModel subject;
    @Mock
    private Application application;
    @Mock
    private PostDataFetcher postDataFetcher;
    @Mock
    private MutableLiveData<List<Post>> mutableLiveData;
    @Mock
    private MutableLiveData<Boolean> errorLiveData;

    @Before
    public void setUp() throws Exception {
        subject = new MainViewModel(application, postDataFetcher, mutableLiveData, errorLiveData);
    }

    @Test
    public void loadLocalData() throws Exception {
        final List<Post> posts = new ArrayList<>();
        posts.add(new Post());
        posts.add(new Post());
        posts.add(new Post());
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<List<Post>> fetcherCallback = (FetcherCallback<List<Post>>) invocation.getArguments()[0];
                fetcherCallback.onDataLoaded(posts);
                return null;
            }
        }).when(postDataFetcher).fetchLocalPostData(Mockito.<FetcherCallback<List<Post>>>any());
        subject.loadLocalData();
        Mockito.verify(postDataFetcher).fetchLocalPostData((FetcherCallback<List<Post>>) Mockito.any());
        Mockito.verify(mutableLiveData).postValue(posts);
    }

    @Test
    public void attemptLoadRemoteData() throws Exception {
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<Void> fetcherCallback = (FetcherCallback<Void>) invocation.getArguments()[0];
                fetcherCallback.onDataLoaded(null);
                return null;
            }
        }).when(postDataFetcher).fetchRemotePostData(Mockito.<FetcherCallback<Void>>any());

        subject.attemptLoadRemoteData();
        subject.attemptLoadRemoteData();
        subject.attemptLoadRemoteData();

        Mockito.verify(postDataFetcher).fetchRemotePostData((FetcherCallback<Void>) Mockito.any());
    }

    @Test
    public void loadRemoteData_success() throws Exception {
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<Void> fetcherCallback = (FetcherCallback<Void>) invocation.getArguments()[0];
                fetcherCallback.onDataLoaded(null);
                return null;
            }
        }).when(postDataFetcher).fetchRemotePostData(Mockito.<FetcherCallback<Void>>any());
        subject.loadRemoteData();
        Mockito.verify(postDataFetcher).fetchRemotePostData(Mockito.<FetcherCallback<Void>>any());
        Mockito.verifyZeroInteractions(errorLiveData);
    }

    @Test
    public void loadRemoteData_error() throws Exception {
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<Void> fetcherCallback = (FetcherCallback<Void>) invocation.getArguments()[0];
                fetcherCallback.onError();
                return null;
            }
        }).when(postDataFetcher).fetchRemotePostData(Mockito.<FetcherCallback<Void>>any());
        subject.loadRemoteData();
        Mockito.verify(postDataFetcher).fetchRemotePostData(Mockito.<FetcherCallback<Void>>any());
        Mockito.verify(errorLiveData).postValue(Mockito.anyBoolean());
    }

    @Test
    public void onCleared() throws Exception {
        subject.onCleared();
        Mockito.verify(postDataFetcher).clearAll();
    }

}