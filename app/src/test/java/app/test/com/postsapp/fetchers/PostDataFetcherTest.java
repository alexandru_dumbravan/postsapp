package app.test.com.postsapp.fetchers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import app.test.com.postsapp.api.model.Post;
import app.test.com.postsapp.api.post.PostApi;
import app.test.com.postsapp.persistence.db.PostDao;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

@RunWith(MockitoJUnitRunner.class)
public class PostDataFetcherTest {

    private PostDataFetcher subject;
    @Mock
    private PostApi postApi;
    @Mock
    private PostDao postDao;

    @Before
    public void setUp() throws Exception {
        subject = new PostDataFetcher(postApi, postDao);
        RxJavaPlugins.reset();
        RxAndroidPlugins.reset();
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });

        RxJavaPlugins.setIoSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Scheduler scheduler) throws Exception {
                return Schedulers.trampoline();
            }
        });


    }

    @Test
    public void fetchRemotePostData_success() throws Exception {
        FetcherCallback<Void> fetcherCallback = Mockito.mock(FetcherCallback.class);
        final List<Post> posts = new ArrayList<>();
        posts.add(new Post());
        posts.add(new Post());
        posts.add(new Post());
        Single<List<Post>> remoteSingle = Single.just(posts);
        Mockito.when(postApi.getPosts()).thenReturn(remoteSingle);

        subject.fetchRemotePostData(fetcherCallback);
        Mockito.verify(postApi).getPosts();
        Mockito.verify(postDao).insertPosts(posts.toArray(new Post[posts.size()]));
    }

    @Test
    public void fetchRemotePostData_error() throws Exception {
        FetcherCallback<Void> fetcherCallback = Mockito.mock(FetcherCallback.class);
        Single<List<Post>> remoteSingle = Single.fromCallable(new Callable<List<Post>>() {
            @Override
            public List<Post> call() throws Exception {
                throw new RuntimeException("test");
            }
        });
        Mockito.when(postApi.getPosts()).thenReturn(remoteSingle);

        subject.fetchRemotePostData(fetcherCallback);
        Mockito.verify(postApi).getPosts();
        Mockito.verify(fetcherCallback).onError();
    }

    @Test
    public void fetchLocalPostData() throws Exception {
        FetcherCallback<List<Post>> fetcherCallback = Mockito.mock(FetcherCallback.class);
        final List<Post> posts = new ArrayList<>();
        posts.add(new Post());
        posts.add(new Post());
        posts.add(new Post());
        Flowable<List<Post>> flowable = Flowable.just(posts);
        Mockito.when(postDao.getAllPosts()).thenReturn(flowable);
        subject.fetchLocalPostData(fetcherCallback);
        Mockito.verify(postDao).getAllPosts();
        Mockito.verify(fetcherCallback).onDataLoaded(posts);
    }

    @Test
    public void fetchPostById() throws Exception {
        FetcherCallback<Post> fetcherCallback = Mockito.mock(FetcherCallback.class);
        Post post = new Post();
        long postId = 1;
        Flowable<Post> flowable = Flowable.just(post);
        Mockito.when(postDao.getPostForId(Mockito.anyInt())).thenReturn(flowable);
        subject.fetchPostById(postId, fetcherCallback);
        Mockito.verify(postDao).getPostForId(postId);
        Mockito.verify(fetcherCallback).onDataLoaded(post);
    }

}