package app.test.com.postsapp.ui.comment;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import app.test.com.postsapp.api.model.Comment;
import app.test.com.postsapp.fetchers.CommentDataFetcher;
import app.test.com.postsapp.fetchers.FetcherCallback;

@RunWith(MockitoJUnitRunner.class)
public class CommentListViewModelTest {

    private CommentListViewModel subject;
    @Mock
    private Application application;
    @Mock
    private CommentDataFetcher commentDataFetcher;
    @Mock
    private MutableLiveData<List<Comment>> mutableLiveData;

    private long postId = 1;

    @Before
    public void setUp() throws Exception {
        subject = new CommentListViewModel(application, commentDataFetcher, mutableLiveData);
    }

    @Test
    public void loadRemoteData() throws Exception {
        subject.loadRemoteData(postId);
        Mockito.verify(commentDataFetcher).fetchRemoteCommentData(Mockito.eq(postId), (FetcherCallback<Void>) Mockito.any());
    }

    @Test
    public void attemptToLoadRemoteData() throws Exception {
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<Void> fetcherCallback = (FetcherCallback<Void>) invocation.getArguments()[1];
                fetcherCallback.onDataLoaded(null);
                return null;
            }
        }).when(commentDataFetcher).fetchRemoteCommentData(Mockito.anyInt(), Mockito.<FetcherCallback<Void>>any());

        subject.attemptToLoadRemoteData(postId);
        subject.attemptToLoadRemoteData(postId);
        subject.attemptToLoadRemoteData(postId);
        Mockito.verify(commentDataFetcher).fetchRemoteCommentData(Mockito.eq(postId), (FetcherCallback<Void>) Mockito.any());
    }

    @Test
    public void loadLocalData() throws Exception {
        final List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                FetcherCallback<List<Comment>> fetcherCallback = (FetcherCallback<List<Comment>>) invocation.getArguments()[1];
                fetcherCallback.onDataLoaded(comments);
                return null;
            }
        }).when(commentDataFetcher).fetchLocalData(Mockito.anyInt(), Mockito.<FetcherCallback<List<Comment>>>any());
        subject.loadLocalData(postId);
        Mockito.verify(commentDataFetcher).fetchLocalData(Mockito.eq(postId), (FetcherCallback<List<Comment>>) Mockito.any());
        Mockito.verify(mutableLiveData).postValue(comments);
    }

    @Test
    public void onCleared() throws Exception {
        subject.onCleared();
        Mockito.verify(commentDataFetcher).clearAll();
    }

}