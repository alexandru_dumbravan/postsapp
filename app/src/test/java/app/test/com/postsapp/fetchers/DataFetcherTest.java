package app.test.com.postsapp.fetchers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.disposables.Disposable;

@RunWith(MockitoJUnitRunner.class)
public class DataFetcherTest {

    private DataFetcher subject;
    @Mock
    private Disposable disposable;

    @Before
    public void setUp() throws Exception {
        subject = new DataFetcher();
        Assert.assertNotNull(subject.disposables);
    }

    @Test
    public void addDisposable() throws Exception {
        subject.addDisposable(disposable);
        Assert.assertEquals(1, subject.disposables.size());
    }

    @Test
    public void clearAll() throws Exception {
        subject.addDisposable(disposable);
        subject.clearAll();
        Mockito.verify(disposable).dispose();
        Assert.assertTrue(subject.disposables.isEmpty());
    }

}